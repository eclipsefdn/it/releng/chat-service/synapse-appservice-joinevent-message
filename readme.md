<!--
SPDX-FileCopyrightText: 2023 eclipse foundation
SPDX-License-Identifier: EPL-2.0
-->

# Synapse Appservice Join Event Message 

This appservice module allows to create a service that can interact with users (local and federated) on a Matrix platform to share information about organization (ie: privacy policy) when joining room.

## configuration

| Configuration Parameter | Description | Default Value | Environment Variable |
| --- | --- | --- | --- |
| `logLevel` | The log level for the service. | `"DEBUG"` | `APP_JOINEVENT_LOG_LEVEL` |
| `bindAddress` | The IP address to bind the service to. | `"0.0.0.0"` | `APP_JOINEVENT_BIND_ADDRESS` |
| `port` | The port to bind the service to. | `9000` | `APP_JOINEVENT_PORT` |
| `homeserverName` | The name of the Matrix homeserver. | `"matrix-local.eclipse.org"` | `APP_JOINEVENT_HOMESERVER_NAME` |
| `homeserverUrl` | The URL of the Matrix homeserver. | `"https://matrix-local.eclipse.org"` | `APP_JOINEVENT_HOMESERVER_URL` |
| `asToken` | The appservice token for authentication. | `"XXXXXXXXXXXX"` | `APP_JOINEVENT_AS_TOKEN` |
| `hsToken` | The homeserver token for authentication. | `"YYYYYYYYYYY"` | `APP_JOINEVENT_HS_TOKEN` |
| `botName` | The name of the bot to use. | `"EF_policy_bot"` | `APP_JOINEVENT_BOT_NAME` |
| `namespace` | override default namespace. | see code  | `APP_JOINEVENT_NAMESPACE` |
| `serverNoticesBot` | The username of the bot that send notices to the server to ignore. | `"eclipsewebmaster"` | `APP_JOINEVENT_SERVER_NOTICES_BOT` |
| `joinRoomAlias` | The name of the room where messages will be sent. | `"Eclipse Foundation Policy Bot"` | `APP_JOINEVENT_ROOM_NAME` |
| `joinMessage` | The message displayed to users when they join the room. | `"You just join `roomAlias`. To continue using Chat Service at Eclipse Foundation you must review and agree to the terms and conditions at <homeserverUrl>/_matrix/consent"` | `APP_JOINEVENT_MESSAGE` |
| `directMessage` | Allow to create or not join room on direct communication. | `false` | `APP_JOINEVENT_DIRECT_MESSAGE` |
| `dataPath` | Store data from appservice. | `false` | `APP_JOINEVENT_DATA_PATH` |


NOTE: `roomAlias` in `joinMessage` is replaced by the first room alias only if the room `history_visibility` is set to `world_readable` otherwise `roomId`is printed.  

## Development

### With docker-compose

Use of `docker-compose.yaml` file.

```shell
docker-compose up
```

### Debug mode configuration with vscode

launch.json

```json
{
    "version": "0.2.0",
    "configurations": [

        {
            "type": "node",
            "request": "attach",
            "name": "Policy bot Docker: Attach to Node",
            "localRoot": "${workspaceFolder}/synapse-appservice-joinevent-message",
            "remoteRoot": "/app",
            "port": 9229,
            "restart": true
        }
    ]
}
```