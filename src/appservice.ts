// SPDX-FileCopyrightText: 2023 eclipse foundation
// SPDX-License-Identifier: EPL-2.0

import {
    Appservice,
    AutojoinRoomsMixin,
    IAppserviceOptions,
    IAppserviceRegistration, LogService,
    SimpleRetryJoinStrategy,
    LogLevel,
    UnstableApis,
    SimpleFsStorageProvider
} from "matrix-bot-sdk";

import * as path from "path";

const morgan = require('morgan')

import { Request, Response, } from "express";

const config = require("config");

LogService.setLevel(LogLevel.fromString(config.get("logLevel"), LogLevel.INFO));

const defaultNamespace = {
    users: [{exclusive: false, regex: `^(?!.*${config.get("serverNoticesBot")}).*$`}],
    rooms: [],
    aliases: [],
};

const registration: IAppserviceRegistration = {
    as_token: config.get("asToken"),
    hs_token: config.get("hsToken"),
    sender_localpart: config.get("botName"),
    namespaces: config.get("namespace") || defaultNamespace,
};

const dataPath = config.get("dataPath")
const storagePath = path.isAbsolute(dataPath) ? dataPath : path.join(__dirname, '../', dataPath);
const storage = new SimpleFsStorageProvider(path.join(storagePath, "bot.json"));

const options: IAppserviceOptions = {
    bindAddress: config.get("bindAddress"),
    port: config.get("port"),
    homeserverName: config.get("homeserverName"),
    homeserverUrl: config.get("homeserverUrl"),

    storage: storage,
    registration: registration,
    joinStrategy: new SimpleRetryJoinStrategy(), 
};

const appserviceBotId = `@${registration.sender_localpart}:${options.homeserverName}`;
const serverNoticesBotId = `@${config.get("serverNoticesBot")}:${options.homeserverName}`;
const guestIdRegex = `^@[0-9]*:${options.homeserverName}`;

const directMessage = config.get("directMessage");

const skipMessage = config.get("skipMessage");

let excludeJoinMessageRegex = config.get("excludeJoinMessageRegex");

if(typeof excludeJoinMessageRegex === "string" && excludeJoinMessageRegex != ""){
    excludeJoinMessageRegex = JSON.parse(excludeJoinMessageRegex);
}

LogService.info(`APP CONFIG:
    logLevel: ${config.get("logLevel")},
    port: ${config.get("port")},
    homeserverName: ${config.get("homeserverName")},
    homeserverUrl: ${config.get("homeserverUrl")},
    asToken: ${config.get("asToken")},
    hsToken: ${config.get("hsToken")},
    botName: ${config.get("botName")},
    namespace: ${config.get("namespace")},
    serverNoticesBot: ${config.get("serverNoticesBot")},
    joinRoomAlias: ${config.get("joinRoomAlias")},
    joinMessage: ${config.get("joinMessage")},
    directMessage: ${config.get("directMessage")},
    skipMessage: ${config.get("skipMessage")},
    excludeJoinMessageRegex: ${config.get("excludeJoinMessageRegex")},
    dataPath: ${config.get("dataPath")}`);

(async () => {

    const appservice = new Appservice(options);
    AutojoinRoomsMixin.setupOnAppservice(appservice);

    const unstableApis = new UnstableApis(appservice.botClient);

    let app = appservice.expressAppInstance;

    app.use(morgan(
        // Same as "combined", but with sensitive values removed from requests.
        ':remote-addr - :remote-user [:date[clf]] ":method :url-safe HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"',
        { stream: { write: LogService.debug.bind(LogService, 'Appservice') } },
    ));

    app.get('/livez', (req: Request, res: Response) => {
        res.send();
    });

    app.get('/readyz', (req: Request, res: Response) => {
        res.send();
    });

    appservice.on("room.event", async(roomId, event) => {

        LogService.trace("room.event", `Received event ${event["event_id"]} (${event["type"]}) from ${event["sender"]} in ${roomId}`);

        if(event.type != "m.room.member")
            return;
        
        // only manage rooms from this homeserver
        if(!roomId.includes(config.get("homeserverName"))){
            LogService.debug(`Skip room ${roomId}, not located in homeserver ${config.get("homeserverName")}`);
            return;
        }

        LogService.debug("room.event of type m.room.member", `Received event ${event?.content?.membership} ${event["event_id"]} (${event["type"]}) from ${event["sender"]} in ${roomId}`);

        let membership = event?.content?.membership;
        if(membership == "join"){
            
            let userJoining = event.sender;
            
            LogService.debug(`User trying to join : ${userJoining}`);

            // appservice bot try to join room
            if(userJoining == appserviceBotId){
                LogService.debug(`Skip appservice bot trying to join : ${userJoining}`);
                return;
            }

            // server notices bot try to join room
            if(userJoining == serverNoticesBotId){
                LogService.debug(`Skip serverNotice bot trying to join : ${userJoining}`);
                return;
            }

            if(userJoining.match(guestIdRegex)){
                LogService.debug(`Skip guest user trying to join : ${userJoining}`);
                return;
            }

            LogService.debug(`Skip users test: ${excludeJoinMessageRegex}:`, Array.isArray(excludeJoinMessageRegex));
            if(excludeJoinMessageRegex && Array.isArray(excludeJoinMessageRegex)){
                LogService.debug(`Skip users join with regexs: ${excludeJoinMessageRegex}`);
                let find = false;
                excludeJoinMessageRegex.forEach((excludeRegex) => {
                    LogService.debug(`Skip users join with regex: ${excludeRegex}`);
                    if(userJoining.match(excludeRegex)){
                        LogService.debug(`"Skip users ${userJoining} with regex: ${excludeRegex}`);
                        find = true
                    }
                });
                if (find) 
                    return;
            }

            // join room for user
            if(event.unsigned != null && event.unsigned.prev_sender == appserviceBotId )
                return;
            
            // direct communication
            if(!bool(directMessage) && event?.prev_content?.is_direct && event?.prev_content?.membership == "invite"){
                LogService.info("Skip creating join room on direct message", `${event["sender"]} in ${roomId}`);
                return;
            }

            LogService.debug("Join Event:", event);

            // handle users left and join again
            if(event.prev_content == null || 
                event.prev_content?.membership == "leave" || 
                event.prev_content?.membership == "invite"){

                LogService.info("room.event of Join Event", `Received event ${event["event_id"]} (${event["type"]}) from ${event["sender"]} in ${roomId}`);
                
                if(bool(skipMessage)){
                    LogService.info("Skip creating join room", `${event["sender"]} in ${roomId}`);
                    return;
                }

                try{
                    let roomAlias = roomId;
                    LogService.debug(`Looking for room alias of ${roomId}`);
                    try{
                        const roomAliases = await unstableApis.getRoomAliases(roomId);
                        LogService.debug(`List of room Aliases for room id ${roomId}: ${roomAliases}`);
                        roomAlias = roomAliases.length? roomAliases[0]: roomId;
                    }catch(error:any){
                        LogService.error("Room preview visibility not properly set :", error.message);
                        return;
                    }

                    const joinRoomId = await retry(() => appservice.botClient.dms.getOrCreateDm(userJoining,
                        userId => 
                            appservice.botClient.createRoom({
                                invite: [userId],
                                is_direct: true,
                                preset: "trusted_private_chat",
                                initial_state : [],
                                name: config.get("joinRoomAlias")
                            })
                        ), 3, 1000);

                    LogService.info(`Send message to ${userJoining} on room ${joinRoomId} while accessing ${roomAlias}`);
                    appservice.botClient.sendMessage(joinRoomId, {
                        "msgtype": "m.notice",
                        "body": config.get("joinMessage").replace("roomAlias", roomAlias),
                    });
                }catch(error:any){
                    LogService.error("Error creating join room:", error.message);
                }
            }else{
                LogService.debug("weird state: ", event.prev_content);
            }
        }else{
            LogService.debug("Other membership:", membership);
        }        
    });
   
    appservice.on("room.leave", async(roomId, event) => {
        LogService.debug("room.leave", `Received event ${event["event_id"]} (${event["type"]}) from ${event["sender"]} in ${roomId}`);
    });

    try {
        await appservice.begin();
        LogService.info("App service started!");
        LogService.info("Log Level: ", LogService.level);

        LogService.debug("App Service configuration: ", options);

        LogService.debug("Update DM!");
        await appservice.botClient.dms.update();

    } catch (error:any) {
        LogService.error("App Crash!!!", error);
    }
})()

async function retry<T>(func: () => Promise<T>, maxRetries: number, delayMs: number): Promise<T> {
    try {
        const result = await func();
        return result;
    } catch (error) {
        LogService.error("Retry!!!", maxRetries, ", error:", error);
        if (maxRetries <= 0) {
        throw error;
        }
        await new Promise(resolve => setTimeout(resolve, delayMs));
        return retry(func, maxRetries - 1, delayMs);
    }
}

function bool(v:any){ return v==="false" ? false : !!v; }